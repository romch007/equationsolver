package net.chardiny.romch007.equationsolver.parser;

import net.chardiny.romch007.equationsolver.equations.SimpleEquation;
import net.chardiny.romch007.equationsolver.terms.FractionTerm;
import net.chardiny.romch007.equationsolver.terms.XTerm;

import java.util.List;

public class EquationParser {
    public SimpleEquation se;
    public EquationParser(String s) {
        String firstPart = s.split("=")[0];
        String secondPart = s.split("=")[1];
        ExpressionParser first = new ExpressionParser(firstPart);
        ExpressionParser second = new ExpressionParser(secondPart);

        second.getXterm().forEach(XTerm::negate);
        second.getFractionTerms().forEach(FractionTerm::negate);

        List<FractionTerm> leftHandedFt = first.getFractionTerms();
        leftHandedFt.addAll(second.getFractionTerms());

        List<XTerm> leftHandedXt = first.getXterm();
        leftHandedXt.addAll(second.getXterm());
        FractionTerm ftSum = new FractionTerm(0);
        for (FractionTerm ft : leftHandedFt) {
            ftSum = ftSum.addTo(ft);
        }
        FractionTerm xtSumTmp = new FractionTerm(0);
        for (XTerm xTerm : leftHandedXt) {
            xtSumTmp = xtSumTmp.addTo(xTerm.getFactor());
        }
        XTerm xtSum = new XTerm(xtSumTmp);
        se = new SimpleEquation(
                xtSum.getFactor(),
                ftSum
        );
    }

    public SimpleEquation getSe() {
        return se;
    }
}
