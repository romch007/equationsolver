package net.chardiny.romch007.equationsolver.parser;

import net.chardiny.romch007.equationsolver.exceptions.FractionTermMatchException;
import net.chardiny.romch007.equationsolver.exceptions.XTermMatchException;
import net.chardiny.romch007.equationsolver.terms.FractionTerm;
import net.chardiny.romch007.equationsolver.terms.XTerm;

import java.util.ArrayList;
import java.util.List;

public class ExpressionParser {
    private List<XTerm> xterm;
    private List<FractionTerm> fractionTerms;
    public ExpressionParser(String s) {
        List<Integer> pos = new ArrayList<>();
        List<String> exp = new ArrayList<>();
        for (int cur1 = 0; cur1 < s.length(); cur1++) {
            char c = s.charAt(cur1);
            if (c == '-' || c == '+') {
                pos.add(cur1);
            }
        }
        for (int i = 0; i < pos.size(); i++) {
            try {
                exp.add(s.substring(pos.get(i), pos.get(i + 1)));
            } catch (IndexOutOfBoundsException e) {
                exp.add(s.substring(pos.get(i)));
                break;
            }
        }
        xterm = new ArrayList<>();
        fractionTerms = new ArrayList<>();
        for (String ex : exp) {
            if (ex.charAt(ex.length() - 1) != 'x') {
                try {
                    fractionTerms.add(FractionTerm.parse(ex));
                } catch (FractionTermMatchException e1) {
                    e1.printStackTrace();
                }
            } else {
                try {
                    xterm.add(XTerm.parse(ex));
                } catch (XTermMatchException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<FractionTerm> getFractionTerms() {
        return fractionTerms;
    }

    public List<XTerm> getXterm() {
        return xterm;
    }
}
