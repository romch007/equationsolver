package net.chardiny.romch007.equationsolver.menus;

import java.util.Scanner;

public class MainMenu {

    public MainMenu() {
        Scanner sc = new Scanner(System.in);
        byte choice = 0;
        do {
            System.out.println("1- Simple equation: ax + b = 0");
            System.out.println("2- Null product equation: (ax + b)(cx + d) = 0");
            System.out.println("5- Exit");
            System.out.print("Choice: ");
            choice = sc.nextByte();
            switch (choice) {
                case 1:
                    new SimpleEquationMenu();
                    break;
                case 2:
                    new NullProductEquationMenu();
                    break;
            }
        } while (choice != 5);
    }
    public static void separator() {
        System.out.println("----------------");
    }
}
