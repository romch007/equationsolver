package net.chardiny.romch007.equationsolver.menus;

import net.chardiny.romch007.equationsolver.equations.NullProductEquation;
import net.chardiny.romch007.equationsolver.exceptions.FractionTermMatchException;
import net.chardiny.romch007.equationsolver.terms.FractionTerm;

import java.util.Scanner;

public class NullProductEquationMenu {

    public NullProductEquationMenu() {
        Scanner sc = new Scanner(System.in);
        String aS, bS, cS, dS;
        FractionTerm a, b, c, d;
        MainMenu.separator();
        System.out.println("(ax + b)(cx + d) = 0");
        System.out.print("a = ");
        aS = sc.next();
        System.out.print("b = ");
        bS = sc.next();
        System.out.print("c = ");
        cS = sc.next();
        System.out.print("d = ");
        dS = sc.next();

        try {
            a = FractionTerm.parse(aS);
            try {
                b = FractionTerm.parse(bS);
                try {
                    c = FractionTerm.parse(cS);
                    try {
                        d = FractionTerm.parse(dS);
                        System.out.println(new NullProductEquation(
                                a,
                                b,
                                c,
                                d
                        ).getSolutions().toString());
                        MainMenu.separator();
                    } catch (FractionTermMatchException e) {
                        e.printStackTrace();
                    }
                } catch (FractionTermMatchException e) {
                    e.printStackTrace();
                }
            } catch (FractionTermMatchException e) {
                e.printStackTrace();
            }
        } catch (FractionTermMatchException e) {
            e.printStackTrace();
        }
    }
}
