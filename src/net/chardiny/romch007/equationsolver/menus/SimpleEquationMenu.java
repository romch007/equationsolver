package net.chardiny.romch007.equationsolver.menus;

import net.chardiny.romch007.equationsolver.equations.SimpleEquation;
import net.chardiny.romch007.equationsolver.exceptions.FractionTermMatchException;
import net.chardiny.romch007.equationsolver.terms.FractionTerm;

import java.util.Scanner;

public class SimpleEquationMenu {

    public SimpleEquationMenu() {
        Scanner sc = new Scanner(System.in);
        String aS, bS;
        FractionTerm a, b;
        MainMenu.separator();
        System.out.println("ax + b = 0");
        System.out.print("a = ");
        aS = sc.next();
        System.out.print("b = ");
        bS = sc.next();

        try {
            a = FractionTerm.parse(aS);
            try {
                b = FractionTerm.parse(bS);
                System.out.println(new SimpleEquation(
                        a,
                        b
                ).getSolutions().toString());
                MainMenu.separator();
            } catch (FractionTermMatchException ee) {
                ee.printStackTrace();
            }
        } catch (FractionTermMatchException e) {
            e.printStackTrace();
        }
    }
}
