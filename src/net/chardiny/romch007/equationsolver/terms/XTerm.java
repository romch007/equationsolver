package net.chardiny.romch007.equationsolver.terms;

import net.chardiny.romch007.equationsolver.exceptions.FractionTermMatchException;
import net.chardiny.romch007.equationsolver.exceptions.XTermMatchException;

import java.util.regex.*;

public class XTerm {

    private FractionTerm factor;
    private FractionTerm power;

    public XTerm() {
        factor = new FractionTerm();
        power = new FractionTerm();
    }
    public XTerm(FractionTerm factor) {
        this.factor = factor;
        power = new FractionTerm();
    }
    public XTerm(FractionTerm factor, FractionTerm power) {
        this.factor = factor;
        this.power = power;
    }
    public void negate() {
        factor = factor.multiplyTo(new FractionTerm(-1));
    }

    public static XTerm parse(String s) throws XTermMatchException {
        Pattern xtermPattern = Pattern.compile("([-]?(\\w+))?x(\\^(\\w+))?");
        Matcher m = xtermPattern.matcher(s);
        if (m.find()) {
            if (m.group(1) == null) {
                return new XTerm();
            }
            if (m.group(3) == null) {
                try {
                    String s2 = m.group(1);
                    return new XTerm(FractionTerm.parse(s2));
                } catch (FractionTermMatchException fractionTermMatchException) {
                    throw new XTermMatchException();
                }
            }
        } else {
            throw new XTermMatchException();
        }
        return new XTerm();
    }

    @Override
    public String toString() {
        return power.approx() == 1 ? factor.toString() + "x" : factor.toString() + "x^" + power.toString();
    }

    public FractionTerm getFactor() {
        return factor;
    }
    public FractionTerm getPower() {
        return power;
    }
}
