package net.chardiny.romch007.equationsolver.terms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SquareRootTerm {

    private FractionTerm factor = new FractionTerm();
    private FractionTerm inroot;

    public SquareRootTerm(FractionTerm inroot) {
        this.inroot = inroot;
    }
    public SquareRootTerm(FractionTerm inroot, FractionTerm factor) {
        this.inroot = inroot;
        this.factor = factor;

    }

    @Override
    public String toString() {
        return factor.approx() == 1 ? "\u221a(" + inroot + ")" : "(" + factor + "\u221a(" + inroot + "))" ;
    }

    public void reduce() {
        List<FractionTerm> divisors = inroot.getDivisors();
        Collections.reverse(divisors);
        for (FractionTerm div : divisors) {
            double sqrt = Math.sqrt(inroot.divideTo(div).approx());
            if ((sqrt == Math.floor(sqrt)) && !Double.isInfinite(sqrt) && sqrt != 1) {
                factor = new FractionTerm((int) sqrt).multiplyTo(factor);
                inroot = div;
                break;
            }
        }
    }

    public boolean isInteger() {
        return Double.isFinite(Math.sqrt(inroot.approx()));
    }

}
