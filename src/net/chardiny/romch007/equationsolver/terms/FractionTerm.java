package net.chardiny.romch007.equationsolver.terms;

import net.chardiny.romch007.equationsolver.exceptions.FractionTermMatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class FractionTerm {

    private int numerator;             // FractionTerm numerator
    private int denominator;           // FractionTerm denominator

    /*-----------------------------------------------------------------
     * constructor
     * Takes no parameters, initializes the object to 0/1
     */
    public FractionTerm() {
        numerator = 1;
        denominator = 1;
    }

    /*-----------------------------------------------------------------
     * constructor
     * Takes parameter, the numerator, initializes denominator to 1
     * so object is numerator/1
     */
    public FractionTerm(int num) {
        numerator = num;
        denominator = 1;
    }

    /*-----------------------------------------------------------------
     * constructor
     * If fraction is negative, put negative number in numerator
     */
    public FractionTerm(int num, int denom) {
        numerator = (denom < 0 ? -num : num);
        if (denom == 0) {
            denominator = 1;
        }
        denominator = (denom < 0 ? -denom : denom);
        reduce();
    }

    /*-----------------------------------------------------------------
     * setNumerator
     * numerator is set to be the given parameter
     */
    public void setNumerator(int num) {
        numerator = num;
        reduce();
    }

    /*-----------------------------------------------------------------
     * getNumerator
     * return numerator
     */
    public int getNumerator() {
        return numerator;
    }

    /*-----------------------------------------------------------------
     * setDenominator
     * denominator is set to be the given parameter (zero is ignored),
     * if denominator is negative, numerator is adjusted
     */
    public void setDenominator(int denom) {
        if (denom > 0) {
            denominator = denom;
            reduce();
        }
        else if (denom < 0) {
            numerator = -numerator;
            denominator = -denom;
            reduce();
        }
    }

    /*-----------------------------------------------------------------
     * getDenominator
     * return denominator
     */
    public int getDenominator() {
        return denominator;
    }

    /*-----------------------------------------------------------------
     * addTo
     * add the parameter FractionTerm to the current object FractionTerm
     */
    public FractionTerm addTo(FractionTerm rhs) {
        FractionTerm sum = new FractionTerm();
        sum.denominator = denominator * rhs.denominator;
        sum.numerator = numerator * rhs.denominator
                + denominator * rhs.numerator;
        sum.reduce();
        return sum;
    }
    public FractionTerm subTo(FractionTerm rhs) {
        FractionTerm sum = new FractionTerm();
        sum.denominator = denominator * rhs.denominator;
        sum.numerator = numerator * rhs.denominator
                - denominator * rhs.numerator;
        sum.reduce();
        return sum;
    }

    public FractionTerm multiplyTo(FractionTerm rhs) {
        FractionTerm product = new FractionTerm();
        product.numerator = numerator * rhs.numerator;
        product.denominator = denominator * rhs.denominator;
        product.reduce();
        return product;
    }
    public FractionTerm divideTo(FractionTerm rhs) {
        FractionTerm quotient = new FractionTerm();
        quotient.numerator = numerator * rhs.denominator;
        quotient.denominator = denominator * rhs.numerator;
        quotient.reduce();
        return quotient;
    }
    public void negate() {
        numerator *= -1;
    }

    public double approx() {
        return (double) numerator/ (double) denominator;
    }

    /*-----------------------------------------------------------------
     * toString
     * convert the FractionTerm to a String object, e.g., 2/3
     */
    public String toString() {
        return denominator != 1 ? "(" + numerator + "/" + denominator + ")" : Integer.toString(numerator);
    }

    /*-----------------------------------------------------------------
     * equals
     * compare the parameter FractionTerm to the current object FractionTerm
     */
    public boolean equals(FractionTerm rhs) {
        return (numerator == rhs.numerator) && (denominator == rhs.denominator);
    }

    /*-----------------------------------------------------------------
     * reduce
     * reduce FractionTerm to lowest terms by finding largest common denominator
     * and dividing it out
     */
    private void reduce() {
        // find the larger of the numerator and denominator
        int n = numerator, d = denominator, largest;
        if (numerator < 0) {
            n = -numerator;
        }
        if (n > d) {
            largest = n;
        }
        else {
            largest = d;
        }

        // find the largest number that divide the numerator and
        // denominator evenly
        int gcd = 0;
        for (int i = largest; i >= 2; i--) {
            if (numerator % i == 0 && denominator % i == 0) {
                gcd = i;
                break;
            }
        }

        // divide the largest common denominator out of numerator, denominator
        if (gcd != 0) {
            numerator /= gcd;
            denominator /= gcd;
        }
        if (numerator < 0 && denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }
    }

    public List<FractionTerm> getDivisors() {
        List<FractionTerm> list = new ArrayList<>();
        if (Double.isFinite(approx())) {
            int n = (int) approx();
            for (int i = 1; i <= n; i++) {
                if (n % i == 0)
                    list.add(new FractionTerm(i));
            }
            return list;
        } else {
            return list;
        }
    }

    public static FractionTerm parse(String s) throws FractionTermMatchException {
        Pattern fractionPattern = Pattern.compile("[-]?\\d+|\\(([-]?\\d+)/(\\d+)\\)");
        Matcher m = fractionPattern.matcher(s);
        if (m.find()) {
            if (m.group(1) == null) {
                return new FractionTerm(Integer.parseInt(m.group(0)));
            } else {
                return new FractionTerm(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            }
        } else {
            throw new FractionTermMatchException("String \"" + s + "\" did not match");
        }
    }
}
