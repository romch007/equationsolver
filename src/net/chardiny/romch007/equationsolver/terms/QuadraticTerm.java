package net.chardiny.romch007.equationsolver.terms;

public class QuadraticTerm {
    private FractionTerm a, b;
    private SquareRootTerm discriminant;

    public QuadraticTerm(FractionTerm b, FractionTerm a, SquareRootTerm discriminant) {
        this.a = a;
        this.b = b;
        this.discriminant = discriminant;
    }

    @Override
    public String toString() {
        return "(" + b.toString() + "\u00b1" + discriminant.toString() + ")/" + a.toString();
    }
}
