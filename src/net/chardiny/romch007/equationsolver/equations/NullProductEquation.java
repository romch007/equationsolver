package net.chardiny.romch007.equationsolver.equations;

import net.chardiny.romch007.equationsolver.terms.FractionTerm;

public class NullProductEquation {
    private FractionTerm a, b, c, d;
    private Solutions solutions;


    /**
     * Equation of the form (ax + b)(cx + d) = 0
     * @param a First term
     * @param b Second term
     * @param c Third term
     * @param d Fourth term
     */
    public NullProductEquation(FractionTerm a, FractionTerm b, FractionTerm c, FractionTerm d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        solutions = new Solutions();
        SimpleEquation firstSe = new SimpleEquation(a, b);
        SimpleEquation secondSe = new SimpleEquation(c, d);
        solutions.addAll(firstSe.getSolutions());
        solutions.addAll(secondSe.getSolutions());
    }

    public FractionTerm getA() {
        return a;
    }

    public FractionTerm getB() {
        return b;
    }

    public FractionTerm getC() {
        return c;
    }

    public FractionTerm getD() {
        return d;
    }
    public Solutions getSolutions() {
        return solutions;
    }
}
