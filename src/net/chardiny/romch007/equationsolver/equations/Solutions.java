package net.chardiny.romch007.equationsolver.equations;

import net.chardiny.romch007.equationsolver.terms.FractionTerm;
import net.chardiny.romch007.equationsolver.terms.QuadraticTerm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Solutions {
    private List<FractionTerm> ftList = new ArrayList<>();
    private List<QuadraticTerm> qtList = new ArrayList<>();


    public void add(FractionTerm ft) {
        ftList.add(ft);
    }
    public void add(QuadraticTerm qt) {
        qtList.add(qt);
    }
    public void addAll(Solutions s) {
        for (FractionTerm ft : s.ftList) {
            add(ft);
        }
        for (QuadraticTerm qt : qtList) {
            add(qt);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("x \u2208 {");
        for (FractionTerm ft : this.ftList) {
            sb.append(ft.toString()).append(", ");
        }
        for (QuadraticTerm qt : this.qtList) {
            sb.append(qt.toString()).append(", ");
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 2);
        }
        sb.append("}");
        return !(ftList.isEmpty() && qtList.isEmpty())  ? String.valueOf(sb) : "x \u2208 {}";
    }

    public Iterator<FractionTerm> ftList() {
        return ftList.iterator();
    }
    public Iterator<QuadraticTerm> qtList() {
        return qtList.iterator();
    }
}
