package net.chardiny.romch007.equationsolver.equations;

import net.chardiny.romch007.equationsolver.terms.FractionTerm;

public class SimpleEquation {

    private Solutions solutions;
    private FractionTerm a, b;

    /**
     * Equation of the form ax + b = 0
     * @param a First term
     * @param b Second term
     */
    public SimpleEquation(FractionTerm a, FractionTerm b) {
        this.a = a;
        this.b = b;
        solutions = new Solutions();
        FractionTerm solution = b.multiplyTo(new FractionTerm(-1)).divideTo(a);
        solutions.add(solution);
    }

    public FractionTerm getA() {
        return a;
    }

    public FractionTerm getB() {
        return b;
    }

    public Solutions getSolutions() {
        return solutions;
    }
}
