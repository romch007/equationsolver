package net.chardiny.romch007.equationsolver.equations;

import net.chardiny.romch007.equationsolver.terms.FractionTerm;
import net.chardiny.romch007.equationsolver.terms.QuadraticTerm;
import net.chardiny.romch007.equationsolver.terms.SquareRootTerm;

public class QuadraticEquation {

    private FractionTerm a, b, c;
    private Solutions solutions;

    /**
     * Equation of form ax² + bx + c = 0
     * @param a First term
     * @param b Second term
     * @param c Third term
     */
    public QuadraticEquation(FractionTerm a, FractionTerm b, FractionTerm c) {
        // b² - 4ac
        double discriminant = Math.pow(b.approx(), 2) - 4 * a.approx() * c.approx();
        solutions = new Solutions();
        if (discriminant == 0) {
            FractionTerm solution = b.multiplyTo(new FractionTerm(-1)).divideTo(a.multiplyTo(new FractionTerm(2)));
            solutions.add(solution);
        } else if (discriminant > 0) {
            QuadraticTerm solution = new QuadraticTerm(
                    b.multiplyTo(new FractionTerm(-1)),
                    a.multiplyTo(new FractionTerm(2)),
                    new SquareRootTerm(
                            b.multiplyTo(b).subTo(a.multiplyTo(c).multiplyTo(new FractionTerm(4)))
                    )
            );
            solutions.add(solution);
        }
    }
    public Solutions getSolutions() {
        return solutions;
    }
}
