package net.chardiny.romch007.equationsolver.exceptions;

public class XTermMatchException extends Exception {
    public XTermMatchException() { super(); }
    public XTermMatchException(String m) { super(m); }
    public XTermMatchException(String m, Throwable cause) { super(m, cause); }
    public XTermMatchException(Throwable cause) { super(cause); }
}
