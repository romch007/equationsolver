package net.chardiny.romch007.equationsolver.exceptions;

public class FractionTermMatchException extends Exception {
    public FractionTermMatchException() { super(); }
    public FractionTermMatchException(String m) { super(m); }
    public FractionTermMatchException(String m, Throwable cause) { super(m, cause); }
    public FractionTermMatchException(Throwable cause) { super(cause); }
}
