package net.chardiny.romch007.equationsolver;

import net.chardiny.romch007.equationsolver.parser.EquationParser;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        EquationParser eq = new EquationParser(sc.next());
        System.out.println(eq.getSe().getSolutions().toString());
    }
}
